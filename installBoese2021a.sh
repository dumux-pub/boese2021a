#!/bin/sh

### Create a folder for the DUNE and DuMuX modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Boese2021a.git dumux-Boese2021a

### Go to specific branches
cd dune-common && git checkout releases/2.8 && cd ..
cd dune-geometry && git checkout releases/2.8 && cd ..
cd dune-grid && git checkout releases/2.8 && cd ..
cd dune-istl && git checkout releases/2.8 && cd ..
cd dune-localfunctions && git checkout releases/2.8 && cd ..
cd dune-foamgrid && git checkout releases/2.8 && cd ..
cd dumux && git checkout master && cd ..
cd dumux-Boese2021a && cd ..

### Go to specific commits
cd dumux && git checkout 5677bcd2ebba77fd140a54c2aeda7e9009ba8676 && cd ..


### Run dunecontrol
./dune-common/bin/dunecontrol --opts=./dumux/cmake.opts all
