// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the  one-phase two-component pore network model.
 */
#ifndef DUMUX_PNM1P2C_PROBLEM_HH
#define DUMUX_PNM1P2C_PROBLEM_HH

// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetwork/1pnc/model.hh>
//#include <dumux/porousmediumflow/1pnc/volumevariables.hh>

#include <dumux/common/boundarytypes.hh>

namespace Dumux
{
  
template <class TypeTag>
class PNMOnePTwoCProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    
    

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    
    using Labels = GetPropType<TypeTag, Properties::Labels>;
    enum {
        // primary variable indices
        conti0EqIdx = Indices::conti0EqIdx,
        transportEqIdx = 1,
        pressureIdx = Indices::pressureIdx,
        //temperatureIdx = Indices::temperatureIdx,
        //energyEqIdx = Indices::energyEqIdx
    };

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<GridView::dimension>::Entity;

public:
    template<class SpatialParams>
    PNMOnePTwoCProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<SpatialParams> spatialParams)
    : ParentType(gridGeometry, spatialParams)
    {
        // get some parameters from the input file
        topPressure_ = getParam<Scalar>("Problem.TopPressure");
        topMassFraction_ = getParam<Scalar>("Problem.TopMassFraction");
        bottomMassFraction_ = getParam<Scalar>("Problem.BottomMassFraction");

        FluidSystem::init();
    }


    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 283.15; } // 10°C
    // \}
     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolume& scv) const
    {
        BoundaryTypes bcTypes;
        if (isTop_(scv))
            bcTypes.setAllDirichlet();
        else if (isBottom_(scv))
        {
            bcTypes.setNeumann(conti0EqIdx);
            bcTypes.setDirichlet(conti0EqIdx+1);
        }

        return bcTypes;
    }
    
        /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element& element,
                               const SubControlVolume& scv
                               /*const ElementVolumeVariables& elemVolVars,*/
                               /*const ElementFluxVariablesCache& elemFluxVarsCache*/) const
    {
        PrimaryVariables values(0.0);
        if (isBottom_(scv)) {
            values[transportEqIdx] = bottomMassFraction_;
            
            
        }
        else
        {   
            //auto d = isTop_(scv) - isBottom_(scv);
            //auto upwindTerm = volVars.density(FluidSystem::gasPhaseIdx);
           //upwindTerm *= volVars.mobility(FluidSystem::gasPhaseIdx);

            //const auto phaseFlux = -1.0 * upwindTerm * volVars.permeability() * (topPressure_ - volVars.pressure(FluidSystem::gasPhaseIdx))/d;

            //if (topPressure_ < volVars.pressure(FluidSystem::gasPhaseIdx))
           //Scalar massFracH2O = volVars.massFraction(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx);

            values[pressureIdx] = topPressure_;
            values[transportEqIdx] = topMassFraction_;
            //values[energyEqIdx] = temperature();
            
            //values[conti0EqIdx] = phaseFlux * massFracH2O;
        }

         return values;
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Vertex& vertex) const
    {
        PrimaryVariables values(0.0);
        values[pressureIdx] = 1e5;
        //values[temperatureIdx] = temperature();
        return values;
    }

private:

    bool isBottom_(const SubControlVolume& scv) const
    {
        return this->gridGeometry().poreLabel(scv.dofIndex()) == 1;
    }

    bool isTop_(const SubControlVolume& scv) const
    {
        return this->gridGeometry().poreLabel(scv.dofIndex()) == 2;
    }

    Scalar topPressure_;
    Scalar topMassFraction_;
    Scalar bottomMassFraction_;
};
} //end namespace Dumux

#endif
