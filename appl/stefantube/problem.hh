// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTwoCTests
 * \brief Non-isothermal gas injection problem where a gas (e.g. air)
 *        is injected into a fully water saturated medium.
 */

#ifndef DUMUX_STEFANTUBE_PROBLEM_HH
#define DUMUX_STEFANTUBE_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>

#include <dumux/material/components/n2.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

/*!
 * \ingroup TwoPTwoCModel
 * \brief Non-isothermal gas injection problem where a gas (e.g. air)
 *        is injected into a fully water saturated medium.
 *
 * During buoyancy driven upward migration the gas passes a high temperature area.
 *
 * The domain is sized 40m times 40m in a depth of 1000m. The rectangular area
 * with the increased temperature (380K) starts at (20m, 1m) and ends at
 * (30m, 30m).
 *
 * For the mass conservation equation Neumann boundary conditions are used on
 * the top and on the bottom of the domain, while Dirichlet conditions
 * apply on the left and the right boundary.
 * For the energy conservation equation Dirichlet boundary conditions are applied
 * on all boundaries.
 *
 * Gas is injected at the bottom boundary from 15m to 25m at a rate of
 * 0.001kg/(s m), the remaining Neumann boundaries are no-flow boundaries.
 *
 * At the Dirichlet boundaries a hydrostatic pressure, a gas saturation of zero and
 * a geothermal temperature gradient of 0.03K/m are applied.
 *
 * The model is able to use either mole or mass fractions. The property useMoles
 * can be set to either true or false in the problem file. Make sure that the
 * according units are used in the problem set-up.
 * The default setting for useMoles is true.
 *
 * This problem uses the \ref TwoPTwoCModel and \ref NIModel model.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2p2cni</tt> or
 * <tt>./test_cc2p2cni</tt>
 *  */
template <class TypeTag >
class StefanTubeProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;

    // primary variable indices
    enum
    {
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx
    };

    // equation indices
    enum
    {
        contiH2OEqIdx = Indices::conti0EqIdx + FluidSystem::H2OIdx,
        contiN2EqIdx = Indices::conti0EqIdx + FluidSystem::N2Idx

    };

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    //! Property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = ModelTraits::useMoles();

public:
    StefanTubeProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {

        FluidSystem::init();

        name_ = getParam<std::string>("Problem.Name");
        extrusionFactor_ = getParam<Scalar>("Problem.ExtrusionFactor");

        //stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout << "The problem uses mole-fractions" << std::endl;
        else
            std::cout << "The problem uses mass-fractions" << std::endl;
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }


    /*!
     * \brief Returns the temperature [K]
     */

    Scalar temperature() const
    { return 283.15; }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;
        bcTypes.setAllNeumann();

        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param elemVolVars The element volume variables
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub-control volume face
     *
     * Negative values mean influx.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        // no-flow everywhere except at the top boundary
        NumEqVector values(0.0);
              const auto& globalPos = scvf.ipGlobal();
        //on the top
        if (globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_)
        {
        values += advectiveFluxes(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf);
        values += diffusiveFluxes(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf);
        }
        values /= extrusionFactor_;
        return values;
      }
//advective Flux inside the stefan tube
    NumEqVector advectiveFluxes(const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const ElementFluxVariablesCache& elemFluxVarsCache,
                            const SubControlVolumeFace& scvf) const
    {
        NumEqVector advValues(0.0);
      const auto& globalPos = scvf.ipGlobal();
      //on the top
      if (globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_)
      {
                 // set a fixed pressure for the gas phase on the top of the domain
            static const Scalar dirichletPressure = 1e5;
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];

            auto d = globalPos[1] - element.geometry().center()[1];

            auto upwindTerm = useMoles ? volVars.molarDensity(FluidSystem::gasPhaseIdx) : volVars.density(FluidSystem::gasPhaseIdx);
            upwindTerm *= volVars.mobility(FluidSystem::gasPhaseIdx);

            const auto phaseFlux = -1.0*upwindTerm*volVars.permeability()*(dirichletPressure - volVars.pressure(FluidSystem::gasPhaseIdx))/d;

            //for the flux of H2O we make an upwind decision depending on the pressure:
            Scalar massFracH2O = 0.0;
        if (dirichletPressure < volVars.pressure(FluidSystem::gasPhaseIdx))

            massFracH2O = volVars.massFraction(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx);

        advValues[contiH2OEqIdx] += phaseFlux*massFracH2O;

            //for the flux of N2 we make an upwind decision depending on the pressure:
            Scalar massFracN2 = 1.0;
            if (dirichletPressure < volVars.pressure(FluidSystem::gasPhaseIdx))
                massFracN2 = volVars.massFraction(FluidSystem::gasPhaseIdx, FluidSystem::N2Idx);
            advValues[contiN2EqIdx] += phaseFlux*massFracN2;

      }  return advValues;
    }

    NumEqVector diffusiveFluxes(const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const ElementFluxVariablesCache& elemFluxVarsCache,
                            const SubControlVolumeFace& scvf) const
    {

         NumEqVector diffValues(0.0);
    const auto& globalPos = scvf.ipGlobal();
    //on the top
    if (globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_)
       {
         const auto& volVars = elemVolVars[scvf.insideScvIdx()];

         auto d = globalPos[1] - element.geometry().center()[1];

        //and now for the water vapour:
    Scalar massFracH2OInside = volVars.massFraction(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx);
    Scalar massFracRefH2O = 0.0;
    Scalar evaporationRate = volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::N2Idx, FluidSystem::H2OIdx)
                             * (massFracH2OInside - massFracRefH2O)
                             / d
                             * volVars.density(FluidSystem::gasPhaseIdx);

     // set Neumann bc values
    diffValues[contiH2OEqIdx] = evaporationRate;
    diffValues[contiN2EqIdx] = -1.0*evaporationRate;

  }   return diffValues;
  }
    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        auto priVars = initial_(globalPos);

        return priVars;
    }

    /*!
     * \brief Returns how much the domain is extruded at a given position.
     *
     * This means the factor by which a lower-dimensional
     * entity needs to be expanded to get a full-dimensional cell.
     */
    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    {

        return extrusionFactor_;
    }


private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);
        priVars.setState(Indices::bothPhases);
        priVars[pressureIdx] = 1e5; //pn
        priVars[switchIdx] = 0.6; //sw
        return priVars;
    }

    static constexpr Scalar eps_ = 1e-2;
    std::string name_;
    Scalar extrusionFactor_;
};

} // end namespace Dumux

#endif
