SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

B. Böse <br>
Analysis of the Stefan flow problem and comparison to an advection-diffusion formulation<br>
Master's Thesis, 2022<br>
Universität Stuttgart

Installation
============

The easiest way to install this module is to create a new directory and download `installBoese2021a.sh` from this repository.

```
mkdir Boese2021a && cd Boese2021a
chmod +x installBoese2021a.sh
./installBoese2021a.sh
```

This should automatically download all necessary modules and check out the correct versions. Afterwards dunecontrol is run.




Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installBoese2021a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Boese2021a/raw/master/installBoese2021a.sh).


After the script has run successfully, you may build all executables

```bash
cd dumux-Boese2021a/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following paths

- appl/stefantube
- appl/stefantube_pnm
- appl/stefantubeNI
